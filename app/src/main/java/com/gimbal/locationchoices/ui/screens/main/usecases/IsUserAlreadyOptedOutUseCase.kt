package com.gimbal.locationchoices.ui.screens.main.usecases

import com.gimbal.locationchoices.api.RestClient
import com.gimbal.locationchoices.ui.base.observer.BaseObservable
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class IsUserAlreadyOptedOutUseCase : BaseObservable<IsUserAlreadyOptedOutUseCase.Listener>() {

    interface Listener {
        fun onUserOptedOut(responseBody: ResponseBody)
        fun onUserOptedIn(exception: String)
    }

    fun isUserAlreadyOptedOut(deviceId: String) {
        RestClient.instance().isOptedOutDeviceId(deviceId).enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.code() < 400) {
                    for (listener in listeners) {
                        listener.onUserOptedOut(response.body()!!)
                    }
                } else {
                    for (listener in listeners) {
                        listener.onUserOptedIn(response.message())
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                for (listener in listeners) {
                    listener.onUserOptedIn(t.printStackTrace().toString())
                }
            }
        })
    }
}
