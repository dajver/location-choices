package com.gimbal.locationchoices.ui.base.holder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerViewHolder<MODEL, LISTENER>(parent: ViewGroup, layoutID: Int)  : RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(layoutID, parent, false)) {
    abstract fun bind(model: MODEL, listener: LISTENER)
}
