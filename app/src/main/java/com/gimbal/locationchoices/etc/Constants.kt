package com.gimbal.locationchoices.etc

object Constants {
    const val DRAWER_MENU_HOW_IT_WORKS = 1
    const val DRAWER_MENU_PRIVACY_POLICY = 2
    const val DRAWER_MENU_TERMS_OF_USE = 3
    const val DRAWER_MENU_ABOUT = 4

    const val HOW_IT_WORKS_FIRST_SCREEN = 0
    const val HOW_IT_WORKS_SECOND_SCREEN = 1
    const val HOW_IT_WORKS_THIRD_SCREEN = 2

    const val INTENT_WEB_VIEW_LINK = "web_view_link"

    const val PRIVACY_POLICY_LINK = "https://locationchoices.org/privacy/"
    const val TERMS_OF_USE_LINK = "https://locationchoices.org/terms/"
    const val ABOUT_LINK = "https://locationchoices.org/#about"
}
