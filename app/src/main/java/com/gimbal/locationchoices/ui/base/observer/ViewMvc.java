package com.gimbal.locationchoices.ui.base.observer;

import android.view.View;

public interface ViewMvc {
    View getRootView();
}
