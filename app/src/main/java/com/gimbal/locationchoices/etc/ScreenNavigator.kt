package com.gimbal.locationchoices.etc

import android.content.Context
import android.content.Intent
import com.gimbal.locationchoices.etc.Constants.INTENT_WEB_VIEW_LINK
import com.gimbal.locationchoices.ui.screens.howitworks.HowItWorksActivity
import com.gimbal.locationchoices.ui.screens.main.MainActivity
import com.gimbal.locationchoices.ui.screens.webview.WebViewActivity

class ScreenNavigator(context: Context) {

    private var mContext: Context? = null

    init {
        mContext = context
    }

    fun showGuideView() {
        mContext?.startActivity(Intent(mContext, HowItWorksActivity::class.java))
    }

    fun showHomeView() {
        val intent = Intent(mContext, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        mContext?.startActivity(intent)
    }

    fun showStaticView(link: String) {
        val intent = Intent(mContext, WebViewActivity::class.java)
        intent.putExtra(INTENT_WEB_VIEW_LINK, link)
        mContext?.startActivity(intent)
    }
}
