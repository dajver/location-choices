package com.gimbal.locationchoices.etc

import android.content.Context
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GaidRetriever(private val context: Context) {

    suspend fun getAdvertisementId(): String = withContext(Dispatchers.IO) {
        val advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(context)
        return@withContext if (advertisingIdInfo.isLimitAdTrackingEnabled) {
            UNKNOWN_GAID
        } else {
            advertisingIdInfo.id
        }
    }

    companion object {
        const val UNKNOWN_GAID = "00000000-0000-0000-0000-000000000000"
    }
}
