package com.gimbal.locationchoices.ui.base.observer

interface ObservableMvc<LISTENER_CLASS> : ViewMvc {

    fun registerListener(listener: LISTENER_CLASS)

    fun unregisterListener(listener: LISTENER_CLASS)
}
