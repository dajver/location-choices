package com.gimbal.locationchoices.ui.drawer.model

class DrawerModel(title: String) {

    var mTitle: String? = null

    init {
        mTitle = title
    }
}
