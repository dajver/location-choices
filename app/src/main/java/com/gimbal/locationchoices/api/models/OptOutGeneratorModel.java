package com.gimbal.locationchoices.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OptOutGeneratorModel {

    @SerializedName("device_id")
    @Expose
    private String deviceId;

    public OptOutGeneratorModel(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
