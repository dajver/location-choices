package com.gimbal.locationchoices.ui.base.adapter

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder

abstract class BaseRecyclerAdapter : RecyclerView.Adapter<ViewHolder?>()
