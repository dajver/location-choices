package com.gimbal.locationchoices.ui.screens.howitworks.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.gimbal.locationchoices.R
import com.gimbal.locationchoices.ui.screens.howitworks.adapter.model.HowItWorksModel
import kotlinx.android.synthetic.main.item_pager_how_it_works.view.*

class HowItWorksPageAdapter(private val mContext: Context, pages: List<HowItWorksModel>?) : PagerAdapter() {

    var mPages: List<HowItWorksModel>? = null

    init {
        mPages = pages
    }

    override fun instantiateItem(collection: View, position: Int): Any {
        val view: View = LayoutInflater.from(mContext).inflate(R.layout.item_pager_how_it_works, null)
        view.text1.text = mPages!![position].getText1()
        view.text2.text = mPages!![position].getText2()
        view.image.setImageResource(mPages!![position].getIcon())
        view.main_view.setBackgroundColor(mContext.getColor(mPages!![position].getBackgroundColor()))

        (collection as ViewPager).addView(view, 0)
        return view
    }

    override fun destroyItem(collection: View, position: Int, `view`: Any) {
        (collection as ViewPager).removeView(view as View?)
    }

    override fun getCount(): Int {
        return mPages!!.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }
}
