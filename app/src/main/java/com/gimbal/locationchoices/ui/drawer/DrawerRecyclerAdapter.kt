package com.gimbal.locationchoices.ui.drawer

import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gimbal.locationchoices.R
import com.gimbal.locationchoices.ui.base.adapter.BaseRecyclerAdapter
import com.gimbal.locationchoices.ui.base.holder.BaseRecyclerViewHolder
import com.gimbal.locationchoices.ui.drawer.model.DrawerModel
import kotlinx.android.synthetic.main.item_drawer.view.*

open class DrawerRecyclerAdapter(private val mListener: Listener, private val mDrawerItemList: List<DrawerModel>) : BaseRecyclerAdapter() {

    interface Listener {
        fun onDrawerItemClick(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return DrawerItemHolderBase(parent,
            R.layout.item_drawer
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as DrawerItemHolderBase
        viewHolder.bind(mDrawerItemList[position], mListener)
    }

    override fun getItemCount(): Int {
        return mDrawerItemList.size
    }

    inner class DrawerItemHolderBase(parent: ViewGroup, layoutId: Int): BaseRecyclerViewHolder<DrawerModel, Listener>(parent, layoutId) {
        override fun bind(model: DrawerModel, listener: Listener) {
            itemView.drawer_title.text = model.mTitle
            itemView.setOnClickListener { listener.onDrawerItemClick(adapterPosition) }
        }
    }
}
