package com.gimbal.locationchoices.ui.screens.webview

import android.app.Activity
import android.content.Context
import android.view.View
import android.webkit.WebSettings
import com.gimbal.locationchoices.etc.Constants
import kotlinx.android.synthetic.main.fragment_webview.view.*

class WebViewViewMvcImpl(context: Context, view: View) : WebViewViewMvc {

    private var mController: WebViewViewMvc.Controller? = null

    init {
        mController = WebViewController(this)

        val settings: WebSettings = view.web_view.settings
        settings.builtInZoomControls = true
        settings.pluginState = WebSettings.PluginState.ON
        settings.javaScriptEnabled = true
        settings.setSupportZoom(true)

        view.web_view.loadUrl((context as Activity).intent.extras!!.getString(Constants.INTENT_WEB_VIEW_LINK))
    }
}
