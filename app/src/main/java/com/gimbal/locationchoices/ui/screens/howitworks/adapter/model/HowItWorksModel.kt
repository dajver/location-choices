package com.gimbal.locationchoices.ui.screens.howitworks.adapter.model

class HowItWorksModel(text1: String, text2: String, buttonText: String, icon: Int, backgroundColor: Int) {

    private var mText1: String? = null
    private var mText2: String? = null
    private var mButtonText: String? = null
    private var mIcon: Int? = null
    private var mBackgroundColor: Int? = null

    init {
        mText1 = text1
        mText2 = text2
        mButtonText = buttonText
        mIcon = icon
        mBackgroundColor = backgroundColor
    }

    fun getText1() : String {
        return mText1!!
    }

    fun getText2() : String {
        return mText2!!
    }

    fun getButtonText() : String {
        return mButtonText!!
    }

    fun getIcon() : Int {
        return mIcon!!
    }

    fun getBackgroundColor() : Int {
        return mBackgroundColor!!
    }
}
