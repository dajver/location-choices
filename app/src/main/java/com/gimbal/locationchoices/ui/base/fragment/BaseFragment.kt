package com.gimbal.locationchoices.ui.base.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(getViewId(), container, false)
        onCreateView(savedInstanceState, root)
        return root
    }

    abstract fun getViewId(): Int
    abstract fun onCreateView(savedInstanceState: Bundle?, view: View?)
}
