package com.gimbal.locationchoices.ui.screens.main

import android.os.Bundle
import android.view.View
import com.gimbal.locationchoices.R
import com.gimbal.locationchoices.ui.base.fragment.BaseFragment

class MainFragment : BaseFragment() {

    private var mViewMvc: MainViewMvc? = null

    override fun getViewId(): Int {
        return R.layout.fragment_main
    }

    override fun onCreateView(savedInstanceState: Bundle?, view: View?) {
        mViewMvc = MainViewMvcImpl(context!!, view!!, viewLifecycleOwner)
    }

    override fun onResume() {
        super.onResume()
        mViewMvc?.onUpdateGaidState()
    }
}
