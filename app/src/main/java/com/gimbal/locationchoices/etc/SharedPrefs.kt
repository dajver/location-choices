package com.gimbal.locationchoices.etc

import android.content.Context
import androidx.preference.PreferenceManager
import com.gimbal.locationchoices.etc.GaidRetriever.Companion.UNKNOWN_GAID

object SharedPrefs {

    private const val SHARED_PREFS_IS_FIRST_RUN = "is_first_run"
    private const val SHARED_PREFS_DEVICE_GAID = "user_gaid"

    fun setFirstRunTrue(context: Context) {
        PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
            .putBoolean(SHARED_PREFS_IS_FIRST_RUN, true)
            .apply()
    }

    fun isFirstRun(context: Context): Boolean {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getBoolean(SHARED_PREFS_IS_FIRST_RUN, false)
    }

    fun setGaid(context: Context, userGaid: String) {
        PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
            .putString(SHARED_PREFS_DEVICE_GAID, userGaid)
            .apply()
    }

    fun getGaid(context: Context): String {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(SHARED_PREFS_DEVICE_GAID, UNKNOWN_GAID)!!
    }
}
