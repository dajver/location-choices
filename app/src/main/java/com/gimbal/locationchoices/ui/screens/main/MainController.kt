package com.gimbal.locationchoices.ui.screens.main

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import com.gimbal.locationchoices.api.models.OptOutGeneratorModel
import com.gimbal.locationchoices.ui.screens.main.usecases.OptInDeviceIdUseCase
import com.gimbal.locationchoices.ui.screens.main.usecases.FetchGaidUseCase
import com.gimbal.locationchoices.ui.screens.main.usecases.IsUserAlreadyOptedOutUseCase
import com.gimbal.locationchoices.ui.screens.main.usecases.OptOutDeviceIdUseCase
import okhttp3.ResponseBody

class MainController(private val mView: MainViewMvc, mContext: Context, mLifecycleOwner: LifecycleOwner) : MainViewMvc.Controller,
    FetchGaidUseCase.Listener,
    OptOutDeviceIdUseCase.Listener,
    OptInDeviceIdUseCase.Listener,
    IsUserAlreadyOptedOutUseCase.Listener {

    private var mFetchGaidUseCase: FetchGaidUseCase? = null
    private var mOptOutDeviceIdUseCase: OptOutDeviceIdUseCase? = null
    private var mOptInDeviceIdUseCase: OptInDeviceIdUseCase? = null
    private var mIsUserAlreadyOptedOutUseCase: IsUserAlreadyOptedOutUseCase? = null

    init {
        mFetchGaidUseCase = FetchGaidUseCase(mContext, mLifecycleOwner)
        mFetchGaidUseCase?.registerListener(this)

        mOptOutDeviceIdUseCase = OptOutDeviceIdUseCase()
        mOptOutDeviceIdUseCase?.registerListener(this)

        mOptInDeviceIdUseCase = OptInDeviceIdUseCase()
        mOptInDeviceIdUseCase?.registerListener(this)

        mIsUserAlreadyOptedOutUseCase = IsUserAlreadyOptedOutUseCase()
        mIsUserAlreadyOptedOutUseCase!!.registerListener(this)
    }

    override fun getGaid() {
        mFetchGaidUseCase?.getGaid()
    }

    override fun onGaidFetched(gaid: String) {
        mView.onGaidFetched(gaid)
    }

    override fun isUserOptedOut(deviceId: String) {
        mIsUserAlreadyOptedOutUseCase?.isUserAlreadyOptedOut(deviceId)
    }

    override fun optIn(deviceId: String) {
        val optModel =
            OptOutGeneratorModel(deviceId)
        mOptOutDeviceIdUseCase?.optOut(optModel)
    }

    override fun onOptOutSuccess() {
        mView.onOptOutSuccess()
    }

    override fun onOptOutFailure(exception: String) {
        mView.onOptOutFailure(exception)
    }

    override fun optOut(deviceId: String) {
        mOptInDeviceIdUseCase?.optIn(deviceId)
    }

    override fun onOptedInSuccess() {
        mView.onOptedInSuccess()
    }

    override fun onOptedInFailure(exception: String) {
        mView.onOptedInFailure(exception)
    }

    override fun onUserOptedOut(responseBody: ResponseBody) {
        mView.onUserOptedOut(responseBody)
    }

    override fun onUserOptedIn(exception: String) {
        mView.onUserOptedIn(exception)
    }
}
