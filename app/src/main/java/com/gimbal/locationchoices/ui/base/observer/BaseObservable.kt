package com.gimbal.locationchoices.ui.base.observer

import java.util.*

abstract class BaseObservable<LISTENER_CLASS> : BaseMvc(),
    ObservableMvc<LISTENER_CLASS> {

    private val mListeners = HashSet<LISTENER_CLASS>()

    protected val listeners: Set<LISTENER_CLASS>
        get() = Collections.unmodifiableSet(mListeners)

    override fun registerListener(listener: LISTENER_CLASS) {
        mListeners.add(listener)
    }

    override fun unregisterListener(listener: LISTENER_CLASS) {
        mListeners.remove(listener)
    }
}
