package com.gimbal.locationchoices.ui.base.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getViewId())
        onCreate()
    }

    abstract fun getViewId() : Int
    abstract fun onCreate()
}
