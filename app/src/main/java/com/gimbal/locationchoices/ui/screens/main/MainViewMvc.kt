package com.gimbal.locationchoices.ui.screens.main

import okhttp3.ResponseBody

interface MainViewMvc {

    interface Controller {
        fun getGaid()
        fun isUserOptedOut(deviceId: String)
        fun optIn(deviceId: String)
        fun optOut(deviceId: String)
    }

    fun onGaidFetched(gaid: String)
    fun onUserOptedOut(responseBody: ResponseBody)
    fun onUserOptedIn(exception: String)
    fun onOptOutSuccess()
    fun onOptOutFailure(exception: String)
    fun onOptedInSuccess()
    fun onOptedInFailure(exception: String)
    fun onUpdateGaidState()
}
