package com.gimbal.locationchoices.ui.screens.webview

import android.os.Bundle
import android.view.View
import com.gimbal.locationchoices.R
import com.gimbal.locationchoices.ui.base.fragment.BaseFragment

class WebViewFragment : BaseFragment() {

    private var mViewMvc: WebViewViewMvc? = null

    override fun getViewId(): Int {
        return R.layout.fragment_webview
    }

    override fun onCreateView(savedInstanceState: Bundle?, view: View?) {
        mViewMvc = WebViewViewMvcImpl(context!!, view!!)
    }
}
