package com.gimbal.locationchoices.ui.screens.webview

import com.gimbal.locationchoices.R
import com.gimbal.locationchoices.etc.Constants
import com.gimbal.locationchoices.etc.Constants.ABOUT_LINK
import com.gimbal.locationchoices.etc.Constants.PRIVACY_POLICY_LINK
import com.gimbal.locationchoices.etc.Constants.TERMS_OF_USE_LINK
import com.gimbal.locationchoices.ui.base.activity.BaseActivity
import com.gimbal.locationchoices.ui.views.TransparentToolbar
import kotlinx.android.synthetic.main.activity_webview.*

class WebViewActivity : BaseActivity(), TransparentToolbar.NavigateUpListener {

    override fun getViewId(): Int {
        return R.layout.activity_webview
    }

    override fun onCreate() {
        transparent_toolbar.setNavigateUpListener(this)

        when (intent.extras!!.getString(Constants.INTENT_WEB_VIEW_LINK)) {
            PRIVACY_POLICY_LINK -> transparent_toolbar.setTitle(getString(R.string.drawer_privacy_policy))
            TERMS_OF_USE_LINK -> transparent_toolbar.setTitle(getString(R.string.drawer_terms_of_use))
            ABOUT_LINK -> transparent_toolbar.setTitle(getString(R.string.drawer_about))
        }
    }

    override fun onNavigationUpClicked() {
        finish()
    }
}
