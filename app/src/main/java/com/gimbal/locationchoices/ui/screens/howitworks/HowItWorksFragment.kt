package com.gimbal.locationchoices.ui.screens.howitworks

import android.os.Bundle
import android.view.View
import com.gimbal.locationchoices.R
import com.gimbal.locationchoices.ui.base.fragment.BaseFragment

class HowItWorksFragment : BaseFragment() {

    private var mView: HowItWorksViewMvc? = null

    override fun getViewId(): Int {
        return R.layout.fragment_how_it_works
    }

    override fun onCreateView(savedInstanceState: Bundle?, view: View?) {
        mView = HowItWorksViewMvcImpl(context!!, view!!)
    }
}
