package com.gimbal.locationchoices.ui.screens.main.usecases

import android.content.Context
import android.os.Handler
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import com.gimbal.locationchoices.etc.GaidRetriever
import com.gimbal.locationchoices.etc.GaidRetriever.Companion.UNKNOWN_GAID
import com.gimbal.locationchoices.ui.base.observer.BaseObservable
import kotlinx.coroutines.launch

class FetchGaidUseCase(context: Context, private val mViewLifecycleOwner: LifecycleOwner) :
    BaseObservable<FetchGaidUseCase.Listener>() {

    interface Listener {
        fun onGaidFetched(gaid: String)
    }

    private var mGaidRetriever: GaidRetriever? = null
    private var mGaidValue: String? = UNKNOWN_GAID

    init {
        mGaidRetriever = GaidRetriever(context)
    }

    fun getGaid() {
        mViewLifecycleOwner.lifecycleScope.launch {
            mGaidValue = mGaidRetriever?.getAdvertisementId()
        }

        Handler().postDelayed({
            for (listener in listeners) {
                listener.onGaidFetched(mGaidValue!!)
            }
        }, 500)
    }
}
