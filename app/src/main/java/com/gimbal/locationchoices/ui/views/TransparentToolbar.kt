package com.gimbal.locationchoices.ui.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.Toolbar
import com.gimbal.locationchoices.R
import kotlinx.android.synthetic.main.view_toolbar.view.*

open class TransparentToolbar : Toolbar {

    interface NavigateUpListener {
        fun onNavigationUpClicked()
    }

    interface OpenDrawerListener {
        fun onOpenDrawerClicked()
    }

    private var mNavigateUpListener: NavigateUpListener? = null
    private var mOpenDrawerListener: OpenDrawerListener? = null

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    private fun init(context: Context) { View.inflate(context, R.layout.view_toolbar, this)
        setup()
    }

    private fun setup() {
        navigate_up.setOnClickListener { mNavigateUpListener!!.onNavigationUpClicked() }
        open_drawer.setOnClickListener { mOpenDrawerListener!!.onOpenDrawerClicked() }
    }

    fun setTitle(title: String) {
        title_name.text = title
    }

    fun setNavigateUpListener(navigateUpListener: NavigateUpListener) {
        if (mOpenDrawerListener != null) {
            throw RuntimeException("mustn't set multiple listeners")
        }

        mNavigateUpListener = navigateUpListener
        navigate_up.visibility = VISIBLE
        open_drawer.visibility = GONE
        title_name.visibility = VISIBLE
    }

    fun setOpenDrawerListener(openDrawerListener: OpenDrawerListener) {
        if (mNavigateUpListener != null) {
            throw RuntimeException("mustn't set multiple listeners")
        }

        mOpenDrawerListener = openDrawerListener
        navigate_up.visibility = GONE
        open_drawer.visibility = VISIBLE
        title_name.visibility = GONE
    }
}
