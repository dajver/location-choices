package com.gimbal.locationchoices.ui.base.observer

import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.view.View

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.core.app.ActivityCompat

abstract class BaseMvc : ViewMvc {

    private var mRootView: View? = null

    private val context: Context
        get() = rootView!!.context

    protected val resources: Resources
        get() = context.resources

    override fun getRootView(): View? {
        return mRootView
    }

    protected fun setRootView(rootView: View) {
        mRootView = rootView
    }

    protected fun <T : View> findViewById(@IdRes id: Int): T {
        return rootView!!.findViewById(id)
    }

    protected fun getDrawable(@DrawableRes id: Int): Drawable? {
        return ActivityCompat.getDrawable(context, id)
    }

    protected fun getColor(@ColorRes colorId: Int): Int {
        return ActivityCompat.getColor(context, colorId)
    }
}
