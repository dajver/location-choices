package com.gimbal.locationchoices.api

import com.gimbal.locationchoices.api.models.OptOutGeneratorModel
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface API {

    @GET("/opt-out/{deviceId}")
    fun isOptedOutDeviceId(@Path("deviceId") deviceId: String): Call<ResponseBody>

    @POST("/opt-out")
    fun optOutDeviceId(@Body optModel: OptOutGeneratorModel): Call<ResponseBody>

    @DELETE("/opt-out/{deviceId}")
    fun optInDeviceId(@Path("deviceId") deviceId: String): Call<ResponseBody>
}
