package com.gimbal.locationchoices.ui.screens.main.usecases

import com.gimbal.locationchoices.api.RestClient
import com.gimbal.locationchoices.ui.base.observer.BaseObservable
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OptInDeviceIdUseCase : BaseObservable<OptInDeviceIdUseCase.Listener>() {

    interface Listener {
        fun onOptedInSuccess()
        fun onOptedInFailure(exception: String)
    }

    fun optIn(deviceId: String) {
        RestClient.instance().optInDeviceId(deviceId).enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.code() < 400) {
                    for (listener in listeners) {
                        listener.onOptedInSuccess()
                    }
                } else {
                    for (listener in listeners) {
                        listener.onOptedInFailure(response.message())
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                for (listener in listeners) {
                    listener.onOptedInFailure(t.printStackTrace().toString())
                }
            }
        })
    }
}
