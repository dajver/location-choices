package com.gimbal.locationchoices.api

import com.gimbal.locationchoices.BuildConfig
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

class RestClient {

    private val service: API

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(logLevel())
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        service = retrofit.create(API::class.java)
    }

    private fun logLevel(): OkHttpClient {
        val dispatcher = Dispatcher()
        dispatcher.maxRequests = 1

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .dispatcher(dispatcher)
            .build()
    }

    companion object {
        const val BASE_URL = BuildConfig.BASE_URL

        private val instance = RestClient()

        fun instance(): API {
            return instance.service
        }
    }
}