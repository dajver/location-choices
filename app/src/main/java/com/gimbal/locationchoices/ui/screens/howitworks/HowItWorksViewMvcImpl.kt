package com.gimbal.locationchoices.ui.screens.howitworks

import android.content.Context
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.gimbal.locationchoices.R
import com.gimbal.locationchoices.etc.Constants
import com.gimbal.locationchoices.etc.ScreenNavigator
import com.gimbal.locationchoices.ui.screens.howitworks.adapter.HowItWorksPageAdapter
import com.gimbal.locationchoices.ui.screens.howitworks.adapter.model.HowItWorksModel
import kotlinx.android.synthetic.main.fragment_how_it_works.view.*

class HowItWorksViewMvcImpl(private val mContext: Context,
                            private val mView: View) : HowItWorksViewMvc, ViewPager.OnPageChangeListener {

    private var mController: HowItWorksViewMvc.Controller? = null
    private var mScreenNavigator: ScreenNavigator? = null

  init {
        mController = HowItWorksController(this)
        mScreenNavigator = ScreenNavigator(mContext)

        val pages: MutableList<HowItWorksModel> = ArrayList()

        val firstText1 = mContext.getString(R.string.how_it_works_first_screen_text1)
        val firstText2 = mContext.getString(R.string.how_it_works_first_screen_text2)
        val firstButtonText = mContext.getString(R.string.how_it_works_first_screen_button_text)
        val firstBackgroundColor = R.color.background
        val howItWorksModel1 = HowItWorksModel(firstText1, firstText2, firstButtonText, 0, firstBackgroundColor)
        pages.add(howItWorksModel1)

        val secondText1 = ""
        val secondText2 = mContext.getString(R.string.how_it_works_second_screen_text1)
        val secondButtonText = mContext.getString(R.string.how_it_works_second_screen_button_text)
        val secondBackgroundColor = R.color.background2
        val howItWorksModel2 = HowItWorksModel(secondText1, secondText2, secondButtonText, R.drawable.ic_lock, secondBackgroundColor)
        pages.add(howItWorksModel2)

        val thirdText1 = mContext.getString(R.string.how_it_works_third_screen_text1)
        val thirdText2 = mContext.getString(R.string.how_it_works_third_screen_text2)
        val thirdButtonText = mContext.getString(R.string.how_it_works_third_screen_button_text)
        val thirdBackgroundColor = R.color.background3
        val howItWorksModel3 = HowItWorksModel(thirdText1, thirdText2, thirdButtonText, R.drawable.ic_scroll, thirdBackgroundColor)
        pages.add(howItWorksModel3)

        val pagerAdapter = HowItWorksPageAdapter(mContext, pages)
        mView.view_pager.adapter = pagerAdapter
        mView.view_pager.addOnPageChangeListener(this)
        mView.page_indicator.setViewPager(mView.view_pager)

        mView.next_button.setOnClickListener {
            if (mView.view_pager.currentItem != Constants.HOW_IT_WORKS_THIRD_SCREEN) {
                val currPos: Int = mView.view_pager.currentItem
                mView.view_pager.currentItem = currPos + 1
            } else {
              mScreenNavigator?.showHomeView()
            }
        }
    }

    override fun onPageSelected(position: Int) {
        when (position) {
            Constants.HOW_IT_WORKS_FIRST_SCREEN -> {
                mView.next_button.text = mContext.getString(R.string.how_it_works_first_screen_button_text)
                mView.next_button.setBackgroundResource(R.drawable.drawable_button_blue)
            }

            Constants.HOW_IT_WORKS_SECOND_SCREEN -> {
                mView.next_button.text = mContext.getString(R.string.how_it_works_second_screen_button_text)
                mView.next_button.setBackgroundResource(R.drawable.drawable_button_green)
            }

            Constants.HOW_IT_WORKS_THIRD_SCREEN -> {
                mView.next_button.text = mContext.getString(R.string.how_it_works_third_screen_button_text)
                mView.next_button.setBackgroundResource(R.drawable.drawable_button_red)
            }
        }
    }

    override fun onPageScrollStateChanged(state: Int) { }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) { }
}
