# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### Added
- Application to opt out of and back in to location-based advertising
- Special view when the ad tracking is limited at the OS level
- Slide out navigation menu 
- How It Works guide that shows up on initial app load
- Static views for the Privacy Policy, Terms of Use, and About web pages
