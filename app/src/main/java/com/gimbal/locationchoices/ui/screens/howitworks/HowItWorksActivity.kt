package com.gimbal.locationchoices.ui.screens.howitworks

import com.gimbal.locationchoices.R
import com.gimbal.locationchoices.ui.base.activity.BaseActivity
import com.gimbal.locationchoices.ui.views.TransparentToolbar
import kotlinx.android.synthetic.main.activity_how_it_works.*

class HowItWorksActivity : BaseActivity(), TransparentToolbar.NavigateUpListener {

    override fun getViewId(): Int {
        return R.layout.activity_how_it_works
    }

    override fun onCreate() {
        transparent_toolbar.setNavigateUpListener(this)
        transparent_toolbar.setTitle(getString(R.string.drawer_how_it_works))
    }

    override fun onNavigationUpClicked() {
        finish()
    }
}
