package com.gimbal.locationchoices.ui.screens.main

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import com.gimbal.locationchoices.R
import com.gimbal.locationchoices.etc.GaidRetriever
import com.gimbal.locationchoices.etc.SharedPrefs
import kotlinx.android.synthetic.main.fragment_main.view.*
import okhttp3.ResponseBody

class MainViewMvcImpl(private val mContext: Context, private val mView: View, mLifecycleOwner: LifecycleOwner) : MainViewMvc {

    private var mController: MainViewMvc.Controller? = null
    private var mGaid: String? = null
    private var mIsUserFound: Boolean? = false
    private var mIsGaidValid: Boolean? = false

    init {
        mController = MainController(this, mContext, mLifecycleOwner)
        mController?.getGaid()

        mView.gaid.text = SharedPrefs.getGaid(mContext)

        mView.opt_in_out_button.setOnClickListener {
            if (mIsUserFound!!) {
                mController?.optOut(mGaid!!)
            } else {
                mController?.optIn(mGaid!!)
            }
        }
    }

    override fun onGaidFetched(gaid: String) {
        mGaid = gaid

        SharedPrefs.setGaid(mContext, mGaid!!)

        mView.gaid.text = mGaid!!

        if (gaid == GaidRetriever.UNKNOWN_GAID) {
            mIsGaidValid = false

            mView.opt_in_out_button.visibility = View.GONE
            mView.description_text.setBackgroundResource(R.drawable.drawable_background_tracking_disabled)
            mView.description_text.text = mContext.getString(R.string.main_screen_description_opt_out_disabled)
            mView.description_text.setTextColor(mContext.getColor(R.color.description_text_active_red))
        } else {
            mIsGaidValid = true

            if (mController != null) {
                mController?.isUserOptedOut(mGaid!!)
            }
        }
    }

    override fun onUserOptedOut(responseBody: ResponseBody) {
        mIsUserFound = true

        mView.opt_in_out_button.visibility = if(mIsGaidValid!!) View.VISIBLE else View.GONE
        mView.opt_in_out_button.setBackgroundResource(R.drawable.drawable_button_green)
        mView.opt_in_out_button.text = mContext.getString(R.string.main_screen_opt_in_button)
        mView.description_text.setBackgroundResource(R.drawable.drawable_background_description)
        mView.description_text.text = mContext.getString(R.string.main_screen_description_opted_out_text)
        mView.description_text.setTextColor(mContext.getColor(R.color.description_text_active_red))
    }

    override fun onUserOptedIn(exception: String) {
        mIsUserFound = false

        mView.opt_in_out_button.visibility = if(mIsGaidValid!!) View.VISIBLE else View.GONE
        mView.opt_in_out_button.setBackgroundResource(R.drawable.drawable_button_red)
        mView.opt_in_out_button.text = mContext.getString(R.string.main_screen_opt_out_button)
        mView.description_text.background = null
        mView.description_text.text = mContext.getString(R.string.main_screen_description_text)
        mView.description_text.setTextColor(mContext.getColor(R.color.black))
    }

    override fun onOptedInSuccess() {
        mController?.isUserOptedOut(mGaid!!)
    }

    override fun onOptedInFailure(exception: String) {
        Toast.makeText(mContext, mContext.getString(R.string.error_opt_in), Toast.LENGTH_LONG).show()
    }

    override fun onOptOutSuccess() {
        mController?.isUserOptedOut(mGaid!!)
    }

    override fun onOptOutFailure(exception: String) {
        Toast.makeText(mContext, mContext.getString(R.string.error_opt_out), Toast.LENGTH_LONG).show()
    }

    override fun onUpdateGaidState() {
        mController?.getGaid()
    }
}
