package com.gimbal.locationchoices.ui.screens.main

import android.content.res.Configuration
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import com.gimbal.locationchoices.R
import com.gimbal.locationchoices.etc.Constants
import com.gimbal.locationchoices.etc.Constants.ABOUT_LINK
import com.gimbal.locationchoices.etc.Constants.PRIVACY_POLICY_LINK
import com.gimbal.locationchoices.etc.Constants.TERMS_OF_USE_LINK
import com.gimbal.locationchoices.etc.ScreenNavigator
import com.gimbal.locationchoices.etc.SharedPrefs
import com.gimbal.locationchoices.ui.base.activity.BaseActivity
import com.gimbal.locationchoices.ui.drawer.DrawerRecyclerAdapter
import com.gimbal.locationchoices.ui.drawer.model.DrawerModel
import com.gimbal.locationchoices.ui.views.TransparentToolbar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), TransparentToolbar.OpenDrawerListener,
    DrawerRecyclerAdapter.Listener {

    private var mDrawerToggle: ActionBarDrawerToggle? = null
    private var mScreenNavigator: ScreenNavigator? = null

    override fun getViewId(): Int {
        return R.layout.activity_main
    }

    override fun onCreate() {
        mScreenNavigator = ScreenNavigator(this)

        onCreateDrawer()

        if (!SharedPrefs.isFirstRun(this)) {
            mScreenNavigator!!.showGuideView()
            SharedPrefs.setFirstRunTrue(this)
        }
    }

    private fun onCreateDrawer() {
        mDrawerToggle = object : ActionBarDrawerToggle(this, drawer_layout, R.string.app_name, R.string.app_name) {}
        drawer_layout.addDrawerListener(mDrawerToggle as ActionBarDrawerToggle)

        transparent_toolbar.setOpenDrawerListener(this)

        val drawerModels = ArrayList<DrawerModel>()
        drawerModels.add(DrawerModel(getString(R.string.drawer_home)))
        drawerModels.add(DrawerModel(getString(R.string.drawer_how_it_works)))
        drawerModels.add(DrawerModel(getString(R.string.drawer_privacy_policy)))
        drawerModels.add(DrawerModel(getString(R.string.drawer_terms_of_use)))
        drawerModels.add(DrawerModel(getString(R.string.drawer_about)))

        val adapter = DrawerRecyclerAdapter(this, drawerModels)
        recycler_view.adapter = adapter
    }

    override fun onOpenDrawerClicked() {
        drawer_layout.openDrawer(GravityCompat.START)
    }

    override fun onDrawerItemClick(position: Int) {
        when (position) {
            Constants.DRAWER_MENU_HOW_IT_WORKS -> {
                mScreenNavigator?.showGuideView()
            }

            Constants.DRAWER_MENU_PRIVACY_POLICY -> {
                mScreenNavigator?.showStaticView(PRIVACY_POLICY_LINK)
            }

            Constants.DRAWER_MENU_TERMS_OF_USE -> {
                mScreenNavigator?.showStaticView(TERMS_OF_USE_LINK)
            }

            Constants.DRAWER_MENU_ABOUT -> {
                mScreenNavigator?.showStaticView(ABOUT_LINK)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        mDrawerToggle?.onConfigurationChanged(newConfig)
    }
}
