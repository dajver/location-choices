package com.gimbal.locationchoices.ui.screens.main.usecases

import com.gimbal.locationchoices.api.RestClient
import com.gimbal.locationchoices.api.models.OptOutGeneratorModel
import com.gimbal.locationchoices.ui.base.observer.BaseObservable
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OptOutDeviceIdUseCase : BaseObservable<OptOutDeviceIdUseCase.Listener>() {

    interface Listener {
        fun onOptOutSuccess()
        fun onOptOutFailure(exception: String)
    }

    fun optOut(optModel: OptOutGeneratorModel) {
        RestClient.instance().optOutDeviceId(optModel).enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.code() < 400) {
                    for (listener in listeners) {
                        listener.onOptOutSuccess()
                    }
                } else {
                    for (listener in listeners) {
                        listener.onOptOutFailure(response.message())
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                for (listener in listeners) {
                    listener.onOptOutFailure(t.printStackTrace().toString())
                }
            }
        })
    }
}
